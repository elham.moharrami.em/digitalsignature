package com.keygenerator.signer.digitalSigner;

import jakarta.xml.bind.DatatypeConverter;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

@Component
public class KeyMaker {
    private final String startDir = System.getProperty("user.dir");

    public void createKey() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(1024 * 3);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        KeyFactory kf = KeyFactory.getInstance("RSA");

        RSAPrivateCrtKeySpec privateKeySpec = kf.getKeySpec(keyPair.getPrivate(), RSAPrivateCrtKeySpec.class);

        String privateModules = getElement("Modulus", privateKeySpec.getModulus());
        String privateExponent = getElement("Exponent", privateKeySpec.getPublicExponent());
        String p = getElement("P", privateKeySpec.getPrimeP());
        String q = getElement("Q", privateKeySpec.getPrimeQ());
        String dp = getElement("DP", privateKeySpec.getPrimeExponentP());
        String dq = (getElement("DQ", privateKeySpec.getPrimeExponentQ()));
        String inverseQ = getElement("InverseQ", privateKeySpec.getCrtCoefficient());
        String d = getElement("D", privateKeySpec.getPrivateExponent());

        String formattedPrivateKey = "<RSAKeyValue>\n" + privateModules + "\n" + privateExponent
                + "\n" + p + "\n" + q + "\n" + dp + "\n" + dq + "\n" + inverseQ + "\n" + d + "\n" + "</RSAKeyValue>";

        String privateKeyFilePath = "PrivateKey.xml";

        RSAPublicKeySpec publicKeySpec = kf.getKeySpec(keyPair.getPublic(), RSAPublicKeySpec.class);
        String publicModules = getElement("Modulus", publicKeySpec.getModulus());
        String publicExponent = getElement("Exponent", publicKeySpec.getPublicExponent());

        String formattedPublicKey = "<RSAKeyValue>\n" + publicModules + "\n" + publicExponent + "</RSAKeyValue>";

        String publicKeyFilePath = "PublicKey.xml";

        writeToFile(privateKeyFilePath, formattedPrivateKey);
        writeToFile(publicKeyFilePath, formattedPublicKey);
    }

    private void writeToFile(String fileName, String content) throws IOException {
        File dir = new File(this.startDir);
        File file = new File(dir, fileName);
        FileWriter writer = new FileWriter(file);
        writer.write(content);
        writer.flush();
        writer.close();
    }

    private String readFromFile() throws IOException {
        File dir = new File(this.startDir);
        return new String(Files.readAllBytes(Paths.get(dir + "/PrivateKey.xml")), StandardCharsets.UTF_8);
    }

    static String getElement(String name, BigInteger bigInt) {
        byte[] bytesFromBigInt = bigInt.toByteArray();
        String elementContent = Base64.getEncoder().encodeToString(bytesFromBigInt);
        return String.format("<%s>%s</%s>", name, elementContent, name);
    }


    public PrivateKey getPrivateKey() throws ParserConfigurationException, IOException, SAXException,
            NoSuchAlgorithmException, InvalidKeySpecException {
        String s = readFromFile();

        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Element element = db.parse(new ByteArrayInputStream(s.getBytes())).getDocumentElement();
        String[] names = {"Modulus", "Exponent", "D", "P", "Q", "DP", "DQ", "InverseQ"};
        BigInteger[] vals = new BigInteger[names.length];
        for (int i = 0; i < names.length; i++) {
            String v = element.getElementsByTagName(names[i]).item(0).getTextContent();
            vals[i] = new BigInteger(1, DatatypeConverter.parseBase64Binary(v));
        }
        PrivateKey pk = KeyFactory.getInstance("RSA").generatePrivate(
                new RSAPrivateCrtKeySpec(vals[0], vals[1], vals[2], vals[3], vals[4], vals[5], vals[6], vals[7]));
        return pk;
    }
}

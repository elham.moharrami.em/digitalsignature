package com.keygenerator.signer.digitalSigner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class SignKey implements CommandLineRunner {
    private final KeyMaker keyMaker;
    private final Signer signer;

    public SignKey(KeyMaker keyMaker, Signer signer) {
        this.keyMaker = keyMaker;
        this.signer = signer;
    }

    @Override
    public void run(String... args) throws Exception {
        keyMaker.createKey();
        signer.sign("testText");
    }
}

package com.keygenerator.signer.digitalSigner;

import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

@Component
public class Signer {
    private final KeyMaker keyMaker;

    public Signer(KeyMaker keyMaker) {
        this.keyMaker = keyMaker;
    }

    public void sign(String stringToSign) throws ParserConfigurationException, IOException, NoSuchAlgorithmException,
            InvalidKeySpecException, SAXException, InvalidKeyException, SignatureException {
        PrivateKey privateKey = keyMaker.getPrivateKey();
        byte[] originalData = stringToSign.getBytes(StandardCharsets.US_ASCII);
        byte[] signedData;

        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(privateKey);
        signature.update(originalData);
        signedData = signature.sign();
        String base64String = Base64.getEncoder().encodeToString(signedData);
        System.out.println(base64String);
    }

}
